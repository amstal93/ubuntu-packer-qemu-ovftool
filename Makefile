# ubuntu-packer-qemu-ovftool

all: help

.PHONY: all help build shell

help:
	@echo "Available targets:"
	@echo
	@echo "  build      build and tag Docker image"
	@echo "  shell      run a shell in Docker image"
	@echo


CUSTOM_VERSION?=1.0
UBUNTU_VERSION?=18.04
PACKER_VERSION?=1.6.6

DOCKER_IMAGE?=ubuntu-packer-qemu-ovftool
DOCKER_TAG?=$(CUSTOM_VERSION)-$(UBUNTU_VERSION)-$(PACKER_VERSION)

DOCKER_USER:=packer
DOCKER_HOME:=/$(USER)
DOCKER_UID:=$(shell id -u)
DOCKER_GID:=$(shell test -e /dev/kvm && stat -c "%g" /dev/kvm || id -g)

DOCKER_BUILD:=docker build \
	$(if $(DOCKER_NO_CACHE),--no-cache,) \
	--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
	--build-arg PACKER_VERSION=$(PACKER_VERSION) \

DOCKER_RUN:=docker run \
	--cap-add NET_ADMIN \
	--device=/dev/kvm:/dev/kvm \
	--device=/dev/net/tun:/dev/net/tun \
	--env HOME=$(DOCKER_HOME) \
	--env USER=$(DOCKER_USER) \
	--user $(DOCKER_UID):$(DOCKER_GID) \
	--volume $(PWD):$(DOCKER_HOME) \
	--workdir $(DOCKER_HOME) \

build:
	$(DOCKER_BUILD) \
		--tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
		.

shell:
	$(DOCKER_RUN) \
		--interactive --tty \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		bash
