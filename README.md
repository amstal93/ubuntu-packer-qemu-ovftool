# ubuntu-packer-qemu-ovftool

A Docker image based on Ubuntu including Packer, QEMU & ovftool.

Tools:
 - [Packer](https://www.packer.io/)
 - [QEMU](https://www.qemu.org/)
 - [VMware Open Virtualization Format Tool](https://code.vmware.com/web/tool/ovf)
 - DOS/FAT tools (dosfstools + mtools)
 - ISO tools (genisoimage)

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`
